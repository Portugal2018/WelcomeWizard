﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GRINGlobal.Client.Common;

namespace WelcomeWizardDemo
{

    interface IGRINGlobalDataWizard
    {
        string FormName { get; }
        DataTable ChangedRecords { get; }
        string PKeyName { get; }
    }

    public partial class WelcomeForm : Form, IGRINGlobalDataWizard
    {

        SharedUtils _sharedUtils;
        DataTable _accession = null;

        string _originalPKeys = string.Empty;
        string _accessionPKeys = string.Empty;
        string _inventoryPKeys = string.Empty;

        public WelcomeForm(string pKeys, SharedUtils sharedUtils)
        {
            InitializeComponent();

            _sharedUtils = sharedUtils;
            _originalPKeys = pKeys;
            
            foreach (string pkeyToken in pKeys.Split(';'))
            {
                switch (pkeyToken.Split('=')[0].Trim().ToUpper())
                {
                    case ":ACCESSIONID":
                        _accessionPKeys = pkeyToken;
                        break;
                    case ":INVENTORYID":
                        _inventoryPKeys = pkeyToken;
                        break;
                }
            }
        }


        public DataTable ChangedRecords
        {
            get
            {
                return null;
            }
        }

        public string FormName
        {
            get
            {
                return "Welcome Wizard";
            }
        }

        public string PKeyName
        {
            get
            {
                return "accession_id";
            }
        }

        private void WelcomeForm_Load(object sender, EventArgs e)
        {
            //Show user info
            label_message.Text = string.Format("Username: {0}, UserSite: {1}", _sharedUtils.Username, _sharedUtils.UserSite);

        }



        private void button_GetWebServiceData_Click(object sender, EventArgs e)
        {
            //Load accession info from gringlobal server
            try
            {
                DataSet response = _sharedUtils.GetWebServiceData("get_accession", _accessionPKeys, 0, 0);

                if (response != null)
                {
                    if (response.Tables.Contains("get_accession"))
                    {
                        _accession = response.Tables["get_accession"];

                        if (_accession.Rows.Count > 0)
                        {
                            DataRow firstAccession = _accession.Rows[0];
                            textBox_AccessionId.Text = firstAccession["accession_id"].ToString();
                            textBox_AccessionNumber.Text = firstAccession["accession_number_part1"].ToString() + " " +
                                                            firstAccession["accession_number_part2"].ToString() + " " +
                                                            firstAccession["accession_number_part3"].ToString();

                            comboBox_AccessionStatus.SelectedValue = firstAccession["status_code"].ToString();
                        }
                    }
                    //show exception table
                    else if (response.Tables.Contains("ExceptionTable") && response.Tables["ExceptionTable"].Rows.Count > 0)
                    {
                        GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("There was an unexpected error retrieving data for {0}.\n\nFull error message:\n{1}", "Get Data Error", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                        ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, "get_accession", response.Tables["ExceptionTable"].Rows[0]["Message"].ToString());
                        ggMessageBox.ShowDialog();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button_Save_Click(object sender, EventArgs e)
        {
            if (_accession != null && _accession.Rows.Count > 0)
            {
                DataRow firstAccession = _accession.Rows[0];
                firstAccession["status_code"] = comboBox_AccessionStatus.SelectedValue;


                DataSet dsChanges = new DataSet();
                dsChanges.Tables.Add(_accession.GetChanges());
                DataSet dsResult = _sharedUtils.SaveWebServiceData(dsChanges);


                #region SyncSavedRecords
                if (dsResult.Tables.Contains("get_accession"))
                {
                    StringBuilder errorMessage = new StringBuilder();
                    int errorCount = 0;

                    foreach (DataRow dr in dsResult.Tables["get_accession"].Rows)
                    {
                        DataRow originalRow = _accession.Rows.Find(dr["OriginalPrimaryKeyID"]);

                        if (dr["SavedAction"].ToString().Equals("Update"))
                        {
                            if (dr["SavedStatus"].ToString() == "Success")
                            {
                                originalRow.AcceptChanges();
                                originalRow.ClearErrors();
                            }
                            else
                            {
                                errorCount++;
                                errorMessage.AppendLine(dr["ExceptionIndex"] + " " + dr["ExceptionMessage"]);
                                //if (originalRow != null) originalRow.RowError = "\t" + dr["ExceptionMessage"].ToString();
                            }
                        }
                    }
                    if (errorCount > 0)
                    {
                        GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("There was an unexpected error saving data for {0}.\n\nFull error message:\n{1}", "Save Data Error", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                        ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, "get_accession", errorMessage.ToString());
                        ggMessageBox.ShowDialog();
                    }
                }
                else if (dsResult.Tables.Contains("ExceptionTable") && dsResult.Tables["ExceptionTable"].Rows.Count > 0)
                {
                    GRINGlobal.Client.Common.GGMessageBox ggMessageBox = new GRINGlobal.Client.Common.GGMessageBox("There was an unexpected error saving data for {0}.\n\nFull error message:\n{1}", "Save Data Error", MessageBoxButtons.OK, MessageBoxDefaultButton.Button1);
                    ggMessageBox.MessageText = string.Format(ggMessageBox.MessageText, "get_accession", dsResult.Tables["ExceptionTable"].Rows[0]["Message"].ToString());
                    ggMessageBox.ShowDialog();
                }
                #endregion
            }
        }

        private void button_GetLocalData_Click(object sender, EventArgs e)
        {
            //Load status combobox items from Local database
            DataTable dtStatus = _sharedUtils.GetLocalData("select value_member, display_member from code_value_lookup where group_name = 'ACCESSION_STATUS'", "");

            comboBox_AccessionStatus.DataSource = dtStatus;
            comboBox_AccessionStatus.DisplayMember = "display_member";
            comboBox_AccessionStatus.ValueMember = "value_member";
            comboBox_AccessionStatus.SelectedItem = null;
        }



       
    }
}
