﻿namespace WelcomeWizardDemo
{
    partial class WelcomeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_Save = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_AccessionId = new System.Windows.Forms.TextBox();
            this.textBox_AccessionNumber = new System.Windows.Forms.TextBox();
            this.comboBox_AccessionStatus = new System.Windows.Forms.ComboBox();
            this.button_GetWebServiceData = new System.Windows.Forms.Button();
            this.button_GetLocalData = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label_message = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button_Save
            // 
            this.button_Save.Location = new System.Drawing.Point(407, 158);
            this.button_Save.Name = "button_Save";
            this.button_Save.Size = new System.Drawing.Size(169, 43);
            this.button_Save.TabIndex = 0;
            this.button_Save.Text = "Save to Central Database";
            this.button_Save.UseVisualStyleBackColor = true;
            this.button_Save.Click += new System.EventHandler(this.button_Save_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(126, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Accession ID";
            // 
            // textBox_AccessionId
            // 
            this.textBox_AccessionId.Location = new System.Drawing.Point(276, 29);
            this.textBox_AccessionId.Name = "textBox_AccessionId";
            this.textBox_AccessionId.Size = new System.Drawing.Size(213, 20);
            this.textBox_AccessionId.TabIndex = 2;
            // 
            // textBox_AccessionNumber
            // 
            this.textBox_AccessionNumber.Location = new System.Drawing.Point(276, 66);
            this.textBox_AccessionNumber.Name = "textBox_AccessionNumber";
            this.textBox_AccessionNumber.Size = new System.Drawing.Size(213, 20);
            this.textBox_AccessionNumber.TabIndex = 3;
            // 
            // comboBox_AccessionStatus
            // 
            this.comboBox_AccessionStatus.FormattingEnabled = true;
            this.comboBox_AccessionStatus.Location = new System.Drawing.Point(276, 104);
            this.comboBox_AccessionStatus.Name = "comboBox_AccessionStatus";
            this.comboBox_AccessionStatus.Size = new System.Drawing.Size(213, 21);
            this.comboBox_AccessionStatus.TabIndex = 4;
            // 
            // button_GetWebServiceData
            // 
            this.button_GetWebServiceData.Location = new System.Drawing.Point(210, 158);
            this.button_GetWebServiceData.Name = "button_GetWebServiceData";
            this.button_GetWebServiceData.Size = new System.Drawing.Size(191, 43);
            this.button_GetWebServiceData.TabIndex = 5;
            this.button_GetWebServiceData.Text = "Get from Central Database (Server)";
            this.button_GetWebServiceData.UseVisualStyleBackColor = true;
            this.button_GetWebServiceData.Click += new System.EventHandler(this.button_GetWebServiceData_Click);
            // 
            // button_GetLocalData
            // 
            this.button_GetLocalData.Location = new System.Drawing.Point(14, 158);
            this.button_GetLocalData.Name = "button_GetLocalData";
            this.button_GetLocalData.Size = new System.Drawing.Size(165, 43);
            this.button_GetLocalData.TabIndex = 6;
            this.button_GetLocalData.Text = "Get from Local Database (PC)";
            this.button_GetLocalData.UseVisualStyleBackColor = true;
            this.button_GetLocalData.Click += new System.EventHandler(this.button_GetLocalData_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(126, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Accession Number";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(126, 107);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Accession Status";
            // 
            // label_message
            // 
            this.label_message.AutoSize = true;
            this.label_message.Location = new System.Drawing.Point(12, 9);
            this.label_message.Name = "label_message";
            this.label_message.Size = new System.Drawing.Size(31, 13);
            this.label_message.TabIndex = 9;
            this.label_message.Text = "Hello";
            // 
            // WelcomeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(579, 212);
            this.Controls.Add(this.label_message);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button_GetLocalData);
            this.Controls.Add(this.button_GetWebServiceData);
            this.Controls.Add(this.comboBox_AccessionStatus);
            this.Controls.Add(this.textBox_AccessionNumber);
            this.Controls.Add(this.textBox_AccessionId);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button_Save);
            this.Name = "WelcomeForm";
            this.Text = "WelcomeForm";
            this.Load += new System.EventHandler(this.WelcomeForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_Save;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_AccessionId;
        private System.Windows.Forms.TextBox textBox_AccessionNumber;
        private System.Windows.Forms.ComboBox comboBox_AccessionStatus;
        private System.Windows.Forms.Button button_GetWebServiceData;
        private System.Windows.Forms.Button button_GetLocalData;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label_message;
    }
}