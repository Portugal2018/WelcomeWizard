## Prerequisites of the project

1. Visual Studio 2015 Community Edition for C#
2. .NET Framework 4.6.2
3. Based on Curator Tool 1.9.8.17
4. The developer PC must support a system type for 64-bit Operating System, x64-based processor
5. For the project 'GRINGlobal.Client.Common' install four NuGet Packages for CrystalDecisions:

    CrystalDecisions.CrystalReports.Engine - version 1.0.0 , 
    CrystalDecisions.ReportSource - version 1.0.0 , 
    CrystalDecisions.Shared - version 1.0.0 , 
    CrystalDecisions.Windows.Forms - version 1.0.0


## How to run the project

1. Download from https://gitlab.com/Portugal2018/WelcomeWizard.git and open the solution GRINGlobal.Client.sln 
2. Rebuild the solution two times if errors are produced then uninstall and install the four NuGet packages for CrystalDecisions
3. Set as StartUp project to GRINGlobal.Client.CuratorTool
4. Run the solution

Note 1: More info in https://www.slideshare.net/edwinrojas507/developing-ct-wizards (pending to upload)
