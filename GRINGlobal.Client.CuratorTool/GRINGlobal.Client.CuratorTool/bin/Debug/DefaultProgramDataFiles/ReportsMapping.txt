1x3_Dymo_Viability_Label.rpt = viability_wizard_get_inventory_viability_label
1x3_Freezer_Label.rpt = get_inventory
1x3_Jar_Lid_Label.rpt = get_inventory
1x3_Prepack_Label.rpt = get_inventory
1x3_Zebra_Viability_Label.rpt = viability_wizard_get_inventory_viability_label
3x3_Extra_Bag_Label.rpt = get_inventory
3x3_Jar_Label.rpt = get_inventory
3x3_Packet_Label.rpt = order_wizard_get_packet_label; order_wizard_get_order_request_item
Cooperator - Users at a Site.rpt = rpt_cooperator_by_site
Order-Packing by Accession Number.rpt = order_packing2
Order-Packing by Accession.rpt = order_packing2
Order-Packing Disclaimers.rpt = order_packing2
Order-Packing General with Origin.rpt = order_packing2
Order-Packing General.rpt = order_packing2
Order-Packing NSGC.rpt = order_packing_nsgc
Order-Picking.rpt = order_packing2

